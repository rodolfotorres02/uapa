from django.db import models


class Cliente(models.Model):
    name = models.CharField(verbose_name='Nombre', max_length=30)
    adress = models.CharField(verbose_name='Direccion', max_length=80)
    phone = models.CharField(verbose_name='Telefono', max_length=13)

    def __str__(self):
        return self.name
