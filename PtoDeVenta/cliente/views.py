from .models import Cliente
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.base import TemplateView


class HomePageView(TemplateView):

    template_name = "cliente/index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class ClienteCreateView(CreateView):
    model = Cliente
    fields = ['name', 'adress', 'phone']
    success_url = "form"


class ClienteList(ListView):
    model = Cliente
    template_name = 'cliente_list.html'
