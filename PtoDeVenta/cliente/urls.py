from django.urls import path
from . import views

urlpatterns = [
    path('form', views.ClienteCreateView.as_view(), name='cliente_form'),
    path('listaclientes', views.ClienteList.as_view(), name='lista_clientes'),
    path('', views.HomePageView.as_view(), name='home'),
]