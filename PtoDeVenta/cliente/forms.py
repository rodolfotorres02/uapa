from django import forms

class ClienteForm(forms.Form):
    name = forms.CharField()
    address = forms.CharField(widget=forms.Textarea)
    phone = forms.CharField()
