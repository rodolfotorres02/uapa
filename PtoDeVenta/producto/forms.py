from django import forms

class ProductoForm(forms.Form):
    name = forms.CharField()
    category = forms.CharField()
    price = forms.DecimalField()
    quantity = forms.IntegerField()
