from django.shortcuts import render
from .models import Producto
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView


class ProductoCreateView(CreateView):
    model = Producto
    fields = ['name', 'category', 'price', 'quantity']
    success_url = "form"


class ProductoList(ListView):
    model = Producto
    template_name = 'producto_list.html'