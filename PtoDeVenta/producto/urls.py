from django.urls import path
from . import views

urlpatterns = [
    path('form', views.ProductoCreateView.as_view(), name='producto_form'),
    path('listaproductos', views.ProductoList.as_view(), name='lista_productos'),
]