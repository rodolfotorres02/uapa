from django.contrib import admin
from .models import PuntoVentaRecord, PuntoVentaRecordDetail
# Register your models here.


class PuntoVentaRecordDetailInline(admin.TabularInline):
    model = PuntoVentaRecordDetail
    readonly_fields = ('total',)


class PuntoVentaRecordAdmin(admin.ModelAdmin):
    inlines = (PuntoVentaRecordDetailInline,)
    fields = ('cliente','total','fecha')


admin.site.register(PuntoVentaRecord, PuntoVentaRecordAdmin)
admin.site.register(PuntoVentaRecordDetail)