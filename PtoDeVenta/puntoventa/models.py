from django.db import models


class PuntoVentaRecord(models.Model):
    cliente = models.ForeignKey('cliente.Cliente',on_delete=models.CASCADE,)
    total = models.DecimalField(decimal_places=2,max_digits=20)
    fecha = models.DateField()

    def __str__(self):
        return str(self.cliente) + '---' + str(self.fecha)


class PuntoVentaRecordDetail(models.Model):
    producto = models.ForeignKey('producto.Producto',on_delete=models.CASCADE,)
    punto_venta_record = models.ForeignKey('PuntoVentaRecord', on_delete=models.CASCADE)
    cantidad = models.DecimalField(max_digits=10, decimal_places=2)
            
    @property
    def total(self):
        return self.cantidad * self.precio
    
    @property
    def precio(self):
        return self.producto.price

    def __str__(self):
        return str(self.producto) + '----' + str(self.cantidad)
    
